﻿using System;
using System.Linq;
using LargestPalindromeProduct.Console;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LargestPalindromeProduct.UnitTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void LargesPalindromeGetLargestPalindromeProductIsWorking()
        {
            var _rand = new Random();
            var randomNumber = Enumerable.Range(101110, _rand.Next(999999)).Max();
            LargesPalindrome.GetLargestPalindromeProduct(randomNumber);
        }
    }
}
