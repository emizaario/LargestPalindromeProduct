#2. Largest palindrome product
> This console App find the largest palindrome made from the product of two 3-digit numbers which is less
than N.


## Installation

Windows:

Just run sln project with VS.

## Usage example

1. the console will waiting for a number of tests you can do.
2. the console wait for a N to get a max palindrome number but less than N.
3. the console print the response and wait for a input to close.


## Release History

* 0.0.1
    * Finished

