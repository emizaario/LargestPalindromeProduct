﻿using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;

namespace LargestPalindromeProduct.Console
{
    public class LargesPalindrome
    {
        public static void GetLargestPalindromeProduct(int number)
        {
            var thePalindromeNumber = Enumerable.Range(100, 900)
                .SelectMany(x => Enumerable.Range(x, 1000 - x)
                .Select(y => x * y))
                .Where(IsPalindrome)
                .Where(c => c < number)
                .Max();

            System.Console.WriteLine(thePalindromeNumber);
        }

        static bool IsPalindrome(int number)
        {
            var s = number.ToString();
            return s.Reverse().SequenceEqual(s);
        }
    }
}