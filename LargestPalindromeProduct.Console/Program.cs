﻿namespace LargestPalindromeProduct.Console
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var inputNumberOfTests = System.Console.ReadLine();
            var theNumberOfTest = ValidateNumberStr(inputNumberOfTests);


            while (theNumberOfTest > 0)
            {
                var theNumber = System.Console.ReadLine();
                var theNumberToCompare = ValidatePalindromeNumberStr(theNumber);

                LargesPalindrome.GetLargestPalindromeProduct(theNumberToCompare);

                theNumberOfTest--;
            }

            System.Console.ReadLine();
        }

        private static int ValidateNumberStr(string input)
        {
            int theNumberOfTest;
            if (!int.TryParse(input, out theNumberOfTest))
                throw new ModelIsNotValidException();


            if (theNumberOfTest <= 0 || theNumberOfTest > 100)
                throw new ModelIsNotValidException();

            return theNumberOfTest;
        }

        private static int ValidatePalindromeNumberStr(string input)
        {
            int theNumber;
            if (!int.TryParse(input, out theNumber))
                throw new ModelIsNotValidException();

            return theNumber;
        }
    }
}
